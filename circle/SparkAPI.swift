//
//  SparkAPI.swift
//  CiscoSpark
//
//  Created by Jorge Bastos on 9/10/16.
//  Copyright © 2016 Jorge Bastos. All rights reserved.
//

import Foundation


enum SparkResult {
    case Error(NSError)
    case Value([String:AnyObject])
}

class SparkAPI {
    private var session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    private var task = NSURLSessionTask()
    private let key: String
    
    init (apiKey: String) {
        self.key = apiKey
    }
    
    /// Post message in a group
    /// - parameters:
    ///     - message: The text message been posted
    ///     - inGroupID: the group id string which the message will be sent
    ///     - completion: the server callback with either a [String: AnyObject] or NSError
    func post(message message: String, inGroupID id: String, completion: (result: SparkResult) -> Void) {
        let request = NSMutableURLRequest(URL: NSURL(string: "https://api.ciscospark.com/v1/messages")!)
        request.HTTPMethod = "POST"
        request.addValue("Bearer YmExZTJmZTktZThjMS00ZTc4LWFkNzgtNTczOGVkODMwNTU3MGI3YTI5MTUtODdl", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        
        let info = ["roomId": id,"text": message]
        
        do {
            let payload = try NSJSONSerialization.dataWithJSONObject(info, options: .PrettyPrinted)
            task = session.uploadTaskWithRequest(request, fromData: payload, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) in
                if error != nil {
                    completion(result: .Error(error!))
                } else {
                    if data != nil {
                        do {
                            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
                            completion(result: .Value(json))
                        } catch {
                            let error = NSError(domain: "Parsing JSON", code: -1, userInfo: [NSLocalizedDescriptionKey: "Could not convert data to JSON"])
                            completion(result: SparkResult.Error(error))
                        }
                    }
                }
            })
            task.resume()
        } catch {
            let error = NSError(domain: "Parsing JSON", code: -1, userInfo: [NSLocalizedDescriptionKey: "Could not convert data to JSON"])
            completion(result: SparkResult.Error(error))
        }
    }
    
    /// Get Messages from group
    /// - parameters:
    ///     - completion: the server callback with either a [String: AnyObject] or NSError
    func getMessagesFrom(groupID: String, completion: (result: SparkResult) -> Void) {
        let request = NSMutableURLRequest(URL: NSURL(string: "https://api.ciscospark.com/v1/messages?roomId=\(groupID)")!)
        request.HTTPMethod = "GET"
        request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
        //        request.addValue(groupID, forHTTPHeaderField: "roomId")
        
        task = session.dataTaskWithRequest(request, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            if error != nil {
                completion(result: .Error(error!))
            } else {
                if data != nil {
                    do {
                        let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
                        completion(result: .Value(json))
                    } catch {
                        let error = NSError(domain: "Parsing JSON", code: -1, userInfo: [NSLocalizedDescriptionKey: "Could not convert data to JSON"])
                        completion(result: SparkResult.Error(error))
                    }
                }
            }
        })
        task.resume()
    }
    
    /// Get the user Chat group list
    /// - parameters:
    ///     - completion: the server callback with either a [String: AnyObject] or NSError
    func getChatGroupList(completion: (result: SparkResult) -> Void) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: "https://api.ciscospark.com/v1/rooms")!)
        request.HTTPMethod = "GET"
        request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
        
        
        task = session.dataTaskWithRequest(request, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            if error != nil {
                completion(result: SparkResult.Error(error!))
            } else {
                if data != nil {
                    do {
                        let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
                        completion(result: SparkResult.Value(json))
                    } catch {
                        let error = NSError(domain: "Parsing JSON", code: -1, userInfo: [NSLocalizedDescriptionKey: "Could not convert data to JSON"])
                        completion(result: SparkResult.Error(error))
                    }
                }
            }
        })
        task.resume()
    }
    
    /// Get Chat group details
    /// - parameters:
    ///     - id: the group id which details are been requested
    ///     - completion: the server callback with either a [String: AnyObject] or NSError
    func getDetailForGroup(id id: String, completion: (result: SparkResult) -> Void) {
        let request = NSMutableURLRequest(URL: NSURL(string: "https://api.ciscospark.com/v1/rooms/\(id)")!)
        request.HTTPMethod = "GET"
        request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
        
        task = session.dataTaskWithRequest(request, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            if error != nil {
                completion(result: .Error(error!))
            } else {
                if data != nil {
                    do {
                        let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
                        completion(result: .Value(json))
                    } catch {
                        let error = NSError(domain: "Parsing JSON", code: -1, userInfo: [NSLocalizedDescriptionKey: "Could not convert data to JSON"])
                        completion(result: SparkResult.Error(error))
                    }
                }
            }
        })
        task.resume()
    }
    
    /// Create Chat group
    /// - parameters:
    ///     - name: the name for the group
    ///     - completion: the server callback with either a [String: AnyObject] or NSError
    func creatChatGroup(name name: String, completion: (result: SparkResult) -> Void) {
        let request = NSMutableURLRequest(URL: NSURL(string: "https://api.ciscospark.com/v1/rooms")!)
        request.HTTPMethod = "POST"
        request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        
        let json = ["title": name]
        
        do {
            let data = try NSJSONSerialization.dataWithJSONObject(json, options: .PrettyPrinted)
            task = session.uploadTaskWithRequest(request, fromData: data, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) in
                if error != nil {
                    completion(result: SparkResult.Error(error!))
                } else {
                    if data != nil {
                        do {
                            let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! [String: AnyObject]
                            completion(result: SparkResult.Value(json))
                        } catch {
                            let error = NSError(domain: "Parsing JSON", code: -1, userInfo: [NSLocalizedDescriptionKey: "Could not convert data to JSON"])
                            completion(result: SparkResult.Error(error))
                        }
                    }
                }
            })
        } catch {
            let error = NSError(domain: "Parsing JSON", code: -1, userInfo: [NSLocalizedDescriptionKey: "Could not convert data to JSON"])
            completion(result: SparkResult.Error(error))
        }
        task.resume()
    }
    
    /// Get User Information
    /// - parameters:
    ///     - completion: the server callback with either a [String: AnyObject] or NSError
    func getUserInformation(completion: (result: SparkResult) -> Void) {
        let request = NSMutableURLRequest(URL: NSURL(string: "https://api.ciscospark.com/v1/people/me")!)
        request.HTTPMethod = "GET"
        request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
        
        task = session.dataTaskWithRequest(request, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) in
            if let error = error {
                print(error)
            } else {
                
                print("Data", data)
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as! [String: AnyObject]
                    print(json)
                } catch let jsonError {
                    print(jsonError)
                }
            }
        })
        task.resume()
    }
}

