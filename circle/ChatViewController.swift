import UIKit
import JSQMessagesViewController
import MobileCoreServices
import SwiftyJSON
import Alamofire
import JSQSystemSoundPlayer


class ChatViewController: JSQMessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var groupMessages = [String]()
    var groupID: String?
    var message: JSQMessage! {
        didSet {
            print(message.text)
            sparkAPI.post(message: message.text, inGroupID: "Y2lzY29zcGFyazovL3VzL1JPT00vNjlhNDNjNTAtNzdmNi0xMWU2LWFmNzktMzUzZmIyOTM1YjBl") { (result) in
                if case .Error(let error) = result {
                    print(error)
                } else {
                    print(result)
                }
            }
        }
    }
    
    @IBOutlet weak var personInfo: UINavigationItem!
    
    let incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImageWithColor(UIColor(red: 10/255, green: 180/255, blue: 230/255, alpha: 1.0))
    let outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImageWithColor(UIColor.grayColor())
    var messages = [JSQMessage]()
    var image = UIImage(named: "John.jpeg")!
    override func viewDidLoad() {
        super.viewDidLoad()
        personInfo.title = ("TechCrunch💻" )
        self.tabBarController?.tabBar.hidden = true
        // Do any additional setup after loading the view, typically from a nib.
        self.setup()
        self.addDemoMessages()
        self.addDemoMessages()
    }
    
  override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        sparkAPI.getMessagesFrom(groupID!) { (result) in
            if case .Error(let error) = result {
                print(error)
            } else {
                if case .Value(let json) = result {
                    if let items = json["items"] as? [[String: AnyObject]] {
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            
                            self.groupMessages = items.map { $0["text"] as! String }.filter { $0 != "" }.reverse()
                            
                            for text in self.groupMessages {
                                print(text)
                                let message = JSQMessage(senderId: self.senderId, displayName: self.senderId, text: text)
                                self.finishSendingMessage()
                                //                                self.messages += [message]
                                self.messages.append(message)
                                self.finishSendingMessage()
                                
                                
                                
                                
                            }
                            self.reloadMessagesView()
                            
                            
                            //                            _ = self.groupMessages.map { JSQMessage(senderId: self.senderId, displayName: self.senderId, text: $0) }
                            print(self.groupMessages)
                            
                        })
                    }
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadMessagesView() {
        self.collectionView?.reloadData()
    }
}

//MARK - Setup
extension ChatViewController {
    func addDemoMessages() {
        
//        for i in 1...10 {
//            let sender = (i%2 == 0) ? "Server" : self.senderId
//            let messageContent = "Message nr. \(i)"
//            let message = JSQMessage(senderId: sender, displayName: sender, text: messageContent)
//            print(message.text)
//            // Post messages on Sparky
//            self.messages += [message]
//            
//            self.finishSendingMessage()
//        }
        self.reloadMessagesView()
    }
    
    func setup() {
        self.senderId = UIDevice.currentDevice().identifierForVendor?.UUIDString
        self.senderDisplayName = UIDevice.currentDevice().identifierForVendor?.UUIDString
    }
}

//MARK - Data Source
extension ChatViewController {
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        let data = self.messages[indexPath.row]
        return data
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, didDeleteMessageAtIndexPath indexPath: NSIndexPath!) {
        self.messages.removeAtIndex(indexPath.row)
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        let data = messages[indexPath.row]
        switch(data.senderId) {
        case self.senderId:
            return self.outgoingBubble
        default:
            return self.incomingBubble
        }
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
}

//MARK - Toolbar
extension ChatViewController {
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
        message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text)
        self.messages += [message]
        self.finishSendingMessage()
        
        
    }
    
    override func didPressAccessoryButton(sender: UIButton!) {
        
    }
       
}