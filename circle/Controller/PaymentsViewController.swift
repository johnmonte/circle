//
//  PaymentsViewController.swift
//  circle
//
//  Created by Jorge Bastos on 9/10/16.
//  Copyright © 2016 John Montejano. All rights reserved.
//

import UIKit

class PaymentsViewController: UITableViewController {
    var squad:[String] = ["Zulwiyoza Putra", "Marquavious Draggon", "Kadeem Palacios", "Jorge Bastos"]
    var values: [String] = ["(Owe's: $2.93)","(Owe's: $10.87)","(Owe's: $23.76)", "(Owe's: $10.00)"]
    var images: [String] = ["Putra","Marq", "Kadeem", "Jorge"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return squad.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! WalletTableViewCell
        cell.profileImageView.image = UIImage(named: images[indexPath.row]) ?? UIImage()
        cell.nameLabel.text = "\(squad[indexPath.row]) - \(values[indexPath.row])"

        
        
        
        
        return cell
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
