//
//  ChatMainVC.swift
//  circle
//
//  Created by Jorge Bastos on 9/10/16.
//  Copyright © 2016 John Montejano. All rights reserved.
//

import UIKit

var sparkAPI = SparkAPI(apiKey: "YmExZTJmZTktZThjMS00ZTc4LWFkNzgtNTczOGVkODMwNTU3MGI3YTI5MTUtODdl")

class ChatMainVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var groups = [[String:AnyObject]]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view.
        sparky.getChatGroupList { (result) in
            if case .Error(let error) = result {
                print(error)
            } else {
                if case .Value(let json) = result {
                    if let items = json["items"] as? [[String: AnyObject]] {
                        dispatch_async(dispatch_get_main_queue(), { 
                            self.groups = items
                            self.tableView.reloadData()
                        })
                    }
                }
            }
        }
        
    }
   override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
    self.tabBarController?.tabBar.hidden = false

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("chatview", forIndexPath: indexPath)
        let group = groups[indexPath.row]
        print(group)
        let groupName = group["title"]
        
        cell.textLabel?.text = groupName as? String ?? "Error"
        return cell
    }
//    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        performSegueWithIdentifier("chatSegue", sender: nil)
//    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let index = tableView.indexPathForSelectedRow?.row
        let group = groups[index!]
        let groupID = group["id"] as? String
        
        if segue.identifier == "showChat" {
            if let cvc = segue.destinationViewController as? ChatViewController, groupID = groupID {
                cvc.groupID = groupID
            }
        }
    }
    
}




