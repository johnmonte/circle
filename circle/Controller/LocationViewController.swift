//
//  LocationViewController.swift
//  circle
//
//  Created by Jorge Bastos on 9/10/16.
//  Copyright © 2016 John Montejano. All rights reserved.
//

import UIKit
import ArcGIS
import QuartzCore
class LocationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AGSMapViewLayerDelegate {
    var people:[String] = ["Putra   (Distance: 0.90 Miles)", "Jorge Bastos   (Distance: 0.75 Miles)","Marquavious Draggon   (Distance: 1.23 Miles)"]
    @IBOutlet weak var mapView: AGSMapView!
    @IBOutlet weak var labelLocated: UILabel!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        let url = NSURL(string: "http://services.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer")
        let tiledLayer = AGSTiledMapServiceLayer(URL: url)
        self.mapView.addMapLayer(tiledLayer, withName: "Basemap Tiled Layer")
        self.mapView.layerDelegate = self
        self.view.sendSubviewToBack(self.mapView)
        labelLocated.layer.zPosition = 1
        labelLocated.layer.cornerRadius = 8
        self.view.bringSubviewToFront(labelLocated)
        self.view.sendSubviewToBack(mapView)
        labelLocated.userInteractionEnabled = true
        
        
        


    }
    func mapViewDidLoad(mapView: AGSMapView!) {
        mapView.locationDisplay.startDataSource()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return people.count
        }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = people[indexPath.row]
        return cell
    }
}
